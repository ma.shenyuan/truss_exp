from truss import display,geom
import numpy as np
import numpy.random as npr
np.set_printoptions(precision=4)

def test():
	x = np.array([0,1,2,0,1,2])
	y = np.array([0,0,0,1,1,1])
	nodes = geom.nodes(x,y)
	nodes.set_kinematic([0,3])
	elements = geom.elements([0,0,1,1,1,1,2,2,3,4],[1,4,2,3,4,5,4,5,4,5])
	
	a = npr.random(elements.Ne)+1.
	a[np.where(elements.node2==5)] = 0.
	
	t = geom.truss(nodes, elements)
	print(npr.random(t.elements.Ne).shape)
	t.set_cross_section_area(a)
	t.set_module_young(1.)
	t.set_density(1.)
	
	t.assemble()
	print(t.K_dof)
	print(t.M_dof)
	
	display.plot_truss(t)
	
	
	
if __name__=="__main__":
	test()
