import numpy as np
from truss import geom

def test():
	x = np.array([0., 1., 0.])
	y = np.array([1., 0., -1.])
	
	nodes = geom.nodes(x,y)
	
	elements = geom.elements([0,1],[1,2])
	print(elements.node1)
	print(elements.node2)
	
if __name__=="__main__":
	test()
