import numpy as np
from truss import geom

def test():
	x = np.array([0., 1., 0.])
	y = np.array([1., 0., -1.])
	nodes = geom.nodes(x,y)
	
	elements = geom.elements([0,1],[1,2])
	
	truss = geom.truss(nodes, elements)
	truss.set_cross_section_area(1.)
	truss.set_density(1.)
	truss.set_module_young(1.)
	
	truss.assemble()
	
	print(truss.K)
	print(truss.K_dof)
if __name__=="__main__":
	test()
