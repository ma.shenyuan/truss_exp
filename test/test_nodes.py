import numpy as np
from truss import geom

def test():
	x = np.array([0., 1., 0.])
	y = np.array([1., 0., -1.])
	
	nodes = geom.nodes(x,y)
	print(nodes.dim)
	print(nodes.dof)
	
	nodes.set_kinematic([0,2])
	print(nodes.dof)
	
if __name__=="__main__":
	test()
