from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

setup(
	name="truss",
	version="0.1",
	package_dir={"":"src"},
	packages=find_packages(where="src")
)
