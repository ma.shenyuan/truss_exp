import numpy as np
import matplotlib.pyplot as plt

import picos as pc

from truss import var,display
np.set_printoptions(precision=3)

def main():
	#material properties
	rho = 1.
	E = 2.5e4
	m = 1
	
	#load frequency
	omega = 15.
	
	#build the truss
	x = np.array([0,3]+[0,1,2,3]*2+[0,3])
	y = np.array([0]*2+[-1]*4+[-2]*4+[-3]*2)
	nodes = var.nodes(x,y)
	nodes.set_kinematic([0,1])
	n1=[0,0,1,1,2,2,2,3,3,4,4,4,4,5,6,6,7,7,8,8,9]
	n2=[2,3,4,5,3,6,7,4,7,5,7,8,9,9,7,10,8,10,9,11,11]
	elements = var.elements(n1,n2)
	
	t = var.truss(nodes, elements)
	t.set_density(rho)
	t.set_module_young(E)
	# ~ display.plot_truss(t)
	
	#build time varying loads
	f_R = np.zeros(2*nodes.Nn)
	f_R[10*2+1]=np.sqrt(0.5)
	f_R[11*2+1]=np.sqrt(0.5)
	f_R = pc.Constant(f_R[nodes.dof])

	#assemble matrices K and M as picos expression
	t.assemble()
	
	#get optimization variables and create expressions for later use
	a = t.a
	peak_power = pc.RealVariable("pp")
	
	K = t.K
	M = t.M
	K_omega = -omega**2*M+K
	
	
	total_mass = (a | t.lengths*t.rho) # (|) for dot product
	LMI = (peak_power & f_R.T ) // (f_R & K_omega) # & and // for horizontal and vertical stacking
	
	#define problem and add constraint
	peak_power_min = pc.Problem()
	peak_power_min.set_objective("min",peak_power)
	
	peak_power_min.add_constraint(a>=0)
	peak_power_min.add_constraint(total_mass<=m)
	peak_power_min.add_constraint(LMI>>0)
	
	#quick visualization of the minimization problem
	print(peak_power_min)
	
	# solve the sdp problem with cvxopt
	sol = peak_power_min.solve(solver="cvxopt")
	
	print(f"minimal peak power : {peak_power.value*omega/2:e}")
	
	display.plot_truss(t)
	
if __name__=="__main__":
	main()
