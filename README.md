Here I will do some numerical experiments for solving peak power minimization under multiple harmonic loads.

An environment file is provided you can install it with conda using command:
>conda env create -f environment.yml

By default the name should be truss_py.

After installing conda environment you should install the package with pip under develop at the root folder of this project:
>pip install -e .

Now you can try the example notebook in example folder, where we consider the peak power minimization of single harmonic without in-phase assumption. The topology of the truss is the same as the example in Heidari paper.

The python script peak_power_heidari.py reproduces the same example of Heidari's paper using the same set of parameters. It uses the in-phase assumption and is solved as linear SDP programming.

A local method (method of moving asymptote) is used to search for local minima of peak power. The implementation of mma in python used in this project is hosted in the repot: 

>https://github.com/arjendeetman/GCMMA-MMA-Python

And the original work of mma is due to:
>@article{svanberg1987method,
  title={The method of moving asymptotes—a new method for structural optimization},
  author={Svanberg, Krister},
  journal={International journal for numerical methods in engineering},
  volume={24},
  number={2},
  pages={359--373},
  year={1987},
  publisher={Wiley Online Library}
}
