import warnings
from math import isclose 

from matplotlib import pyplot as plt
from matplotlib import color_sequences as cs
import numpy as np


def plot_truss(trs, a=None, forces = None, filename=None, default_width = 2., ax=None, **kwargs):
	
	if a is None:
		warnings.warn("value of cross section area not specified, display with default value")
		a = np.ones(trs.elements.Ne)
		a_max = 1.
	else:
		a_max = np.max(a)
		
	if ax is None:
		ax = plt.gca()
	
	#draw the elements
	active_elements_count = 0
	for e,(n1, n2) in enumerate(zip(trs.elements.node1,trs.elements.node2)):
		if np.isclose(a[e], 0.):
			continue
		x = trs.nodes.x[[n1,n2]]
		y = trs.nodes.y[[n1,n2]]
		
		relative_width = a[e]/a_max
		if relative_width >= 1e-3:
			ax.plot(x,y, "b", linewidth=default_width*relative_width, zorder=1)
			active_elements_count+=1
		else:
			continue
	print(f"number of active bars {active_elements_count}")
		
	#draw the nodes
	imposed = ~trs.nodes.dof
	for i in range(trs.nodes.Nn):
		dof = np.array([2*i,2*i+1])
		
		if np.all(trs.nodes.dof[dof]):# if both of the dof are free
			ax.scatter(trs.nodes.x[i], trs.nodes.y[i],marker="o", s=80, color="blue", zorder=2)
		else: 
			ax.scatter(trs.nodes.x[i], trs.nodes.y[i],marker="x", s=80, color="red", zorder=2)
			if hasattr(trs.nodes, "u_imp"):
				ux_id = np.sum(imposed[:2*i])
				uy_id = ux_id+1
				ux,uy = trs.nodes.u_imp[[ux_id,uy_id]]
				x,y = trs.nodes.x[i],trs.nodes.y[i]
				print(x,y,ux,uy)
				if not (isclose(ux,0) and isclose(uy,0)):
					ax.arrow(x,y,np.sign(ux),np.sign(uy), width=.025, color="red", zorder=3)
	
	#draw the forces		
	if not(forces is None):
		for f,c in zip(forces, cs["tab10"][1:]):
			
			g = f.copy()
			g = g.reshape((trs.nodes.Nn,2)).T
			norms = np.linalg.norm(g, axis=0)
			ind = np.logical_not(np.isclose(norms,0))
			g[:,ind]/=norms[ind]
			for x,y,dx,dy in zip(trs.nodes.x[ind],trs.nodes.y[ind],g[0,ind],g[1,ind]):
				ax.arrow(x,y,dx/5,dy/5, width=.025, color=c, zorder=3)
	return ax
