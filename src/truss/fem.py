import numpy as np

_STIFF_SHAPE=np.array([[1,-1],[-1,1]])
_MASS_SHAPE = {1:np.array([[2,0,1,0],[0,2,0,1],[1,0,2,0],[0,1,0,2]])/6,
2:np.eye(4)/2}

class nodes():
	def __init__(self, x,y, dim=2):
		#create the list of x,y coordinates
		self.x = x
		self.y = y
		self.Nn = len(self.x)
		
		self.dim = dim
		self.dof = np.ones(self.dim*self.Nn, dtype="?") #binary array of indicating if the i-th displacement is free

	def set_kinematic(self, ids, u_imp=None):
		"""
			Impose displacement. 
			Ids is the list of affected dof id. 
			u_imp is the value of imposed displacement, if not given, u_imp will be 0 by default.		
		"""
		self.dof[ids] = False
		self.ndof = np.sum(self.dof)
		
		if not(u_imp is None):
			self.u_imp = np.array(u_imp)
			
	def set_boundary(self, ids):
		"""
			Set if the nodes indicated by ids are boundary nodes and thus subject to external forces
		"""
		self.is_boundary = np.zeros(self.dim*self.Nn, dtype='?')
		self.is_boundary[ids] = 1
		
class elements():
	def __init__(self, node1, node2, node3 = None):
		# node1,node2 : the lists of node id, the first and the second node of the elements
		# node3 : if given then we are considering triangular mesh
		self.Ne = len(node1)
		
		if node3 is None:
			self.node1 = np.array(node1, dtype=int)
			self.node2 = np.array(node2, dtype=int)
		else:
			self.connectivity = np.array([[n1,n2,n3] for (n1,n2,n3) in zip(node1,node2,node3)])
		
		
class truss():
	def __init__(self, nodes, elements):
		self.nodes = nodes
		self.elements = elements
		self.geometry()
		
	def set_module_young(self, E):
		if type(E) is float:
			self.E = np.ones(self.elements.Ne)*E
		elif type(E) is np.ndarray:
			if E.size!=self.elements.Ne:
				raise IndexError
			else:
				self.E = E
				
	def set_density(self, rho):
		if type(rho) is float:
			self.rho = np.ones(self.elements.Ne)*rho
		elif type(rho) is np.ndarray:
			if E.size!=self.elements.Ne:
				raise IndexError
			else:
				self.rho = rho	
		
	def geometry(self):
		"""
		Compute the cos-sin vector of every elements
		"""
		delta_x = self.nodes.x[self.elements.node2]-self.nodes.x[self.elements.node1]
		delta_y = self.nodes.y[self.elements.node2]-self.nodes.y[self.elements.node1]
		lengths = np.sqrt(delta_x**2+delta_y**2)
		c = delta_x/lengths
		s = delta_y/lengths
		
		self.lengths = lengths
		self.cs = np.vstack((c,s))
				
	def assemble(self, mass_shape=1):
		
		MS = _MASS_SHAPE[mass_shape] 
		ndof = self.nodes.ndof
		nn = self.nodes.Nn
		dim = self.nodes.dim
		ne = self.elements.Ne
		self.Ke = np.zeros((self.elements.Ne, dim*nn, dim*nn))
		self.Me = np.zeros((self.elements.Ne, dim*nn, dim*nn))
		
		for e,(n1,n2) in enumerate(zip(self.elements.node1,self.elements.node2)):

			E = self.E[e]
			rho = self.rho[e]
			l = self.lengths[e]
			CS = np.kron(_STIFF_SHAPE, np.outer(self.cs[:,e],self.cs[:,e]))
			dof_local = np.array([2*n1,2*n1+1,2*n2,2*n2+1])
			row,col = np.ix_(dof_local, dof_local)
			
			# compute the e-th elementary stiffness and mass matrix
			self.Ke[e,row,col] = E/l*CS
			self.Me[e,row,col] = rho*l*MS
			

class lin_ela():
	def __init__(self, nodes, elements):
		self.nodes = nodes
		self.elements = elements
		self.areas()
	
	def set_stress_strain(self, nu, Dtype = 0):
		"""
		Set the stress strain relations and give the Poisson coefficient nu.
		Dtype = 0 => plane stress
		Dtype = 1 => plane strain
		"""
		
		if isinstance(nu, float):
			self.nu = np.ones(self.elements.Ne)*nu
		else:
			self.nu = np.array(nu)
		
		self.D = np.zeros((self.elements.Ne, 3,3))
		
		if Dtype == 0: #plan stress
			self.D[:,0,0] = 1.
			self.D[:,1,1] = 1.
			self.D[:,0,1] = self.nu
			self.D[:,1,0] = self.nu
			self.D[:,2,2] = (1.-self.nu)*0.5
			self.D /= (1-self.nu**2)[:, None, None]
			
		elif Dtype == 1:
			self.D[:,0,0] = 1.-self.nu
			self.D[:,0,1] = self.nu
			self.D[:,1,0] = self.nu
			self.D[:,1,1] = 1.-self.nu
			self.D[:,2,2] = 0.5-self.nu
			self.D /= ((1+self.nu)*(1-2*self.nu))[:, None, None]
		
		else:
			raise ValueError("Unexpected stress-strain type")
		
	def areas(self):
		self.A = np.zeros(self.elements.Ne)
		
		X = np.ones((3,3))
		for i,c in enumerate(self.elements.connectivity):
			X[:,0] = self.nodes.x[c]
			X[:,1] = self.nodes.y[c]
			self.A[i] = 0.5*np.linalg.det(X)
			
	
