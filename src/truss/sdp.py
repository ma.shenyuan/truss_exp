import numpy as np
import cvxpy as cp

from . import fem
		
class sdp():
	def __init__(self,t,cf,Nh):
		self.t = t
		self.cf = cf
		self.Nh = Nh
		self.mass = cp.Parameter((), "m")
		self.eta = cp.Parameter((), "eta")
		self.a = cp.Variable(self.t.elements.Ne, "a")
		
	def _build_peak_power(self):
		#build the sdp variables of sdp certificate
		N = self.Nh
		
		self.theta = cp.Variable((), "theta")
		self.Q1 = cp.Variable((2*N+1,2*N+1), "Q1", hermitian=True)
		self.Q2 = cp.Variable((2*N+1,2*N+1), "Q2",hermitian=True)
		self.X = cp.Variable((3*N,3*N),"X",hermitian = True)
		
		self.certificates = []
		self.certificates.append(self.theta == cp.trace(self.Q1))
		self.certificates.append(self.theta == cp.trace(self.Q2))
		
		N_ = N-1
		for k in range(1,2*N+1):
			Lambda_mk = np.eye(2*N+1,2*N+1,-k)
			if k < N:
				self.certificates.append(self.X[N_,N_+k]+self.X[N_-k,N_] == cp.trace(Lambda_mk@self.Q1))
				self.certificates.append(-(self.X[N_,N_+k]+self.X[N_-k,N_]) == cp.trace(Lambda_mk@self.Q2))
			else:
				self.certificates.append(self.X[N_,N_+k] == cp.trace(Lambda_mk@self.Q1))
				self.certificates.append(-self.X[N_,N_+k] == cp.trace(Lambda_mk@self.Q2))
				
			
		
	def _build_compl(self, omega):
		"""
		Build the compliance constraint (X & F^* \\ F & L) >> 0
		"""
		D = np.diag([-n**2*omega**2 for n in range(1,self.Nh+1)])
		I = np.eye(self.Nh)
		
		N = self.Nh
		ndof = self.t.nodes.ndof
		dof = self.t.nodes.dof
		ddof = np.ix_(dof, dof)
		
		self.L = sum((a_*(np.kron(D,Me[ddof])+np.kron(I, Ke[ddof])) for a_,Ke,Me in zip(self.a, self.t.Ke, self.t.Me)))
		
		cf = self.cf[dof,:]
		F = np.zeros((ndof*N,3*N), dtype="complex128")
		F[:ndof,:N] = cf[:,::-1]
		F[:ndof,N+1:2*N+1] = cf.conjugate()
		for i in range(1, N):
			F[i*ndof:(i+1)*ndof,:]=np.roll(F[:ndof,:],i,axis=1)
		for i in range(N):
			F[i*ndof:(i+1)*ndof,N-1] *= (i+1)*1j*omega
			
		self.F = cp.Constant(F)
		self.compl = cp.bmat([[self.X,cp.conj(self.F).T],[self.F,self.L]])
		
	def build(self,omega):
		self._build_peak_power()
		self._build_compl(omega)
		
		self.obj = cp.Minimize(self.theta + self.eta * cp.trace(self.X))
		cons = self.certificates+[self.compl >> 0]+[self.a>=0, self.a@(self.t.lengths*self.t.rho)<=self.mass]+[self.Q1>>0,self.Q2>>0]
		
		self.problem = cp.Problem(self.obj, cons)
	
	# ~ def build_multi_harmonic_constraints(self, pattern = None):
		# ~ """
		# ~ Construct the constraints of the peak power minimization. The total mass bound should be given later by the user. 
		
		# ~ Pattern is an optional binary array indicating the loaded dof. When given, we expect better scalability. Pattern's size is same as 2* nodes number, we extract relavent dof in the function (be removing cinamatically constrainted dof).		
		# ~ """
		# ~ Nh = self.nodes.Nh
		# ~ omega = self.nodes.omega[0]
		# ~ ndof = self.nodes.ndof
		# ~ cf = self.nodes.cf[self.nodes.dof,:]

		# ~ D0 = np.eye(Nh)
		# ~ D2 = -np.diag(self.nodes.omega**2)
		# ~ L = sum((ae*(np.kron(D2,Me)+np.kron(D0,Ke)) for (ae, Ke, Me) in zip(self.a, self.Ke, self.Me)))
		# ~ self.L = L
		
		# ~ self.X = pc.HermitianVariable("X", 3*Nh)
		# ~ self.Q1 = pc.HermitianVariable("Q1", 2*Nh+1)
		# ~ self.Q2 = pc.HermitianVariable("Q2", 2*Nh+1)
		# ~ self.theta = pc.RealVariable("theta",1)
		
		# ~ # build the expression of (X F^T // F L)
		# ~ F = np.zeros((self.nodes.ndof*Nh,3*Nh), dtype="complex128")
		# ~ F[:ndof,:Nh] = cf[:,::-1]
		# ~ F[:ndof,Nh+1:2*Nh+1] = cf.conjugate()
		# ~ for i in range(1, Nh):
			# ~ F[i*ndof:(i+1)*ndof,:]=np.roll(F[:ndof,:],i,axis=1)
		# ~ for i in range(Nh):
			# ~ F[i*ndof:(i+1)*ndof,Nh-1] *= (i+1)*1j*omega
			
		# ~ self.F = F
		
		# ~ if pattern is None: #No given pattern, we implement litterally the LMI
			# ~ self.schur = (self.X & F.T.conjugate() ) // (F & L)
		# ~ else:
			# ~ pattern_on_dof = pattern[self.nodes.dof]
			# ~ sub_matrix_dof = np.ix_(pattern_on_dof, pattern_on_dof)
			# ~ pattern_aug = np.kron(np.ones(Nh), pattern_on_dof).astype("?")
		
			# ~ L_tilda = sum((ae*((np.kron(D2,Me[sub_matrix_dof])+np.kron(D0,Ke[sub_matrix_dof]))) for (ae, Ke, Me) in zip(self.a, self.Ke, self.Me)))
			
			# ~ self.schur = (self.X & F[pattern_aug,:].T.conjugate() ) // (F[pattern_aug,:] & L_tilda)
			
			# ~ self.freq = sum((ae*((-self.nodes.omega[-1]**2*Me+Ke)[np.ix_(~pattern_on_dof,~pattern_on_dof)]) for (ae, Ke, Me) in zip(self.a, self.Ke, self.Me)))
			
		# ~ # build the SDP certificate
		# ~ certificates = []
		# ~ certificates.append(self.theta == pc.trace(self.Q1))
		# ~ certificates.append(self.theta == pc.trace(self.Q2))
		
		# ~ for k in range(1,2*Nh+1):
			# ~ Lambda_mk = np.eye(2*Nh+1,2*Nh+1,-k)
			# ~ Nh_ = Nh - 1
			# ~ qk = self.X[Nh_,Nh_+k]
			# ~ if k < Nh:
				# ~ qk += self.X[Nh_-k,Nh_]
			
			# ~ # add sdp certificate
			# ~ certificates.append(qk == (Lambda_mk|self.Q1))
			# ~ certificates.append(-qk == (Lambda_mk|self.Q2))
			# ~ if k == 1:
				# ~ Q1_up = (qk-pc.trace(Lambda_mk*self.Q1))*e
				# ~ Q2_up = (-qk-pc.trace(Lambda_mk*self.Q2))*e
			# ~ else:
				# ~ Q1_up += (qk-pc.trace(Lambda_mk*self.Q1))*e
				# ~ Q2_up += (-qk-pc.trace(Lambda_mk*self.Q2))*e
			
		# ~ Q1_BLOCK = (self.theta-pc.trace(self.Q1) & Q1_up) // (Q1_up.conj.T&self.Q1)
		# ~ Q2_BLOCK = (self.theta-pc.trace(self.Q2) & Q2_up) // (Q2_up.conj.T&self.Q2)
		
		# build the constraints
		# ~ self.cons = []
		# ~ self.cons.append(self.a >= 0)
		# ~ self.cons.append(self.schur >> 0)
		# ~ if not(pattern is None):
			# ~ self.cons.append(self.freq >> 0)
		# ~ self.cons.append(self.Q1 >> 0)
		# ~ self.cons.append(self.Q2 >> 0)
		# ~ self.cons += certificates
		
		
	# ~ def add_constraint_to(self,P):
		# ~ """
		# ~ Add peak power minimization constraint to the input problem P
		# ~ """
		# ~ for c in self.cons:
			# ~ P.add_constraint(c)
		# ~ return P
				

