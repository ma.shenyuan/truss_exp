import numpy as np

def power_function(cf,cv):
	d,Nh = cf.shape
	
	cf_ext = np.zeros((d,4*Nh+1), dtype="complex128")
	cv_ext = np.zeros((d,4*Nh+1), dtype="complex128")
	
	cf_ext[:,1:Nh+1] = cf
	cf_ext[:,-Nh:] = cf[:,::-1].conjugate()
	cv_ext[:,1:Nh+1] = cv
	cv_ext[:,-Nh:] = cv[:,::-1].conjugate()
	
	cf_hat = np.fft.fft(cf_ext, axis=1)
	cv_hat = np.fft.fft(cv_ext, axis=1)
	
	pk_hat = np.sum(cf_hat*cv_hat, axis=0)
	
	pk = np.fft.ifft(pk_hat)
	
	return pk
