import numpy as np
import numpy.linalg as npl

import cvxpy as cp
from scipy.sparse.linalg import gmres

from . import utils
"""
This module implement the function that compute the peak power of a design. The function also returns the gradient (sensitivity) of the peak power with respect to the design variables.
"""


class _max_trigonometry():
	"""
	This class calculate the maximum module of a trigonometric polynomial. A trigonometric polynomial is defined as p(z)=sum_{k=-N}^{N} p_k z^{-k} and it returns always real value as z is in the complex unit circle. Since p is real for any z in the complex unit circle, coefficients p_k must satisfy the symmetric condition : p_{-k}=conj(p_k).
	
	Here we implement a callable object to compute the maximum of |p(z)| over the complex unit circle and its gradient with respect to p_k.
	"""
	def __init__(self,N):
		"""
		N is degree of the trigonometric polynomial. It is given upon initialization and cannot be changed after.
		"""
		self.N = N
		self.p = cp.Parameter((N,), "p")
		self.q = cp.Parameter((N,), "q")
		
		self.theta = cp.Variable(1, "theta")
		self.Q1 = cp.Variable((N+1,N+1), "Q1", hermitian=True)
		self.Q2 = cp.Variable((N+1,N+1), "Q2",hermitian=True)
		
		self._build_problem()
		
	def _build_problem(self):
		cons = [0 == -self.theta + cp.trace(self.Q1), 0 == -self.theta+cp.trace(self.Q2)]
		N = self.N
		for k in range(1,N+1):
			lambda_k = np.eye(N+1,N+1,k)
			cons += [-self.p[k-1]-1j*self.q[k-1] == cp.trace(lambda_k@self.Q1)]
			cons += [self.p[k-1]+1j*self.q[k-1] == cp.trace(lambda_k@self.Q2)]
			
		cons += [self.Q1 >> 0, self.Q2 >> 0]
		
		self.problem = cp.Problem(cp.Minimize(self.theta), cons)
		
	def __call__(self,p,q,sens = True):
		self.p.value = p
		self.q.value = q
		
		sol = self.problem.solve(requires_grad=sens, solver = "SCS", eps_abs=1e-8,eps_rel=1e-8)
		if sens:
			try:
				self.problem.backward()
			except cp.error.SolverError:
				print(sol)
				print(self.problem)
				print(self.p.value)
				print(self.q.value)
				exit()
			return sol, self.p.gradient, self.q.gradient
		else:
			return sol

class peak_power():
	def __init__(self, truss, cf, omega):
		"""
		A callable object that serves as the function of peak power. To initialize this object, provide the truss object, the Fourier sequence of the load and the lowest angular frequency of the load.
		"""
		
		self.truss = truss
		self.cf = cf
		self.cv = np.zeros(self.cf.shape, dtype=np.complex128)
		self.adjoint = np.zeros(self.cf.shape, dtype=np.complex128)
		
		self.omega = omega
		self.Nh = self.cf.shape[1]
		
		self._max_t = _max_trigonometry(2*self.Nh)
		
	def _direct_model(self, a):
		#Direct model computation at given design vector a.
		K = sum((ae*Ke for (ae,Ke) in zip(a, self.truss.Ke)))
		M = sum((ae*Me for (ae,Me) in zip(a, self.truss.Me)))
		
		dof = self.truss.nodes.dof
		for i in range(self.Nh):
			K_omega = -((i+1)*self.omega)**2*M+K
			self.cv[dof,i] = npl.solve(K_omega[np.ix_(dof, dof)],self.cf[dof,i])
			self.cv[dof,i] *= (i+1)*1j*self.omega
		
		self.K = K
		self.M = M
		
	def _adjoint_force(self, gp, gq):
		Nh = self.cf.shape[1]
		cf = self.cf
		
		dpdqk = 0.5 * (gq-1j*gq)
		cf_ext = np.zeros((cf.shape[0], 4*Nh+1), dtype=np.complex128)
		dpdqk_ext = np.zeros(4*Nh+1, dtype=np.complex128)
		
		cf_ext[:,1:Nh+1] = cf
		cf_ext[:,-Nh:] = cf[:,::-1].conjugate()
		dpdqk_ext[1:2*Nh+1] = dpdqk
		dpdqk_ext[-2*Nh:] = dpdqk[::-1].conjugate()
		
		cf_fft = np.fft.fft(cf_ext, axis=1)
		dpdqk_fft = np.fft.fft(dpdqk_ext)
		
		adjoint_force_hat = dpdqk_fft[None,:]*cf_fft
		adjoint_force = np.fft.ifft(adjoint_force_hat, axis=1)[:,1:Nh+1]
		return adjoint_force
	
	def _adjoint_model(self,a,gp,gq):
		
		dof = self.truss.nodes.dof
		K = self.K[np.ix_(dof, dof)]
		M = self.M[np.ix_(dof, dof)]
		cg = self._adjoint_force(gp, gq)
		
		for i in range(self.Nh):
			K_omega = -((i+1)*self.omega)**2*M+K
			self.adjoint[dof, i] = npl.solve(K_omega,cg[dof,i])
				
	def __call__(self, a, sens = False):
		#compute the direct model, the nodal velocity Fourier sequence is saved in self.cv
		self._direct_model(a)
		
		#build the coefficients of power function
		pk = utils.power_function(self.cf, self.cv)
		pk = np.where(np.isclose(pk, 0), 0, pk) #make small value 0
		
		p = pk.real[1:2*self.Nh+1]
		q = pk.imag[1:2*self.Nh+1]
		if sens:
			peak_power, gp, gq = self._max_t(p,q, sens = sens)
		else:
			peak_power = self._max_t(p,q,sens = sens)
			return peak_power
			
		#compute the adjoint model
		self._adjoint_model(a,gp,gq)
		#compute gradient using adjoint model
		grad = np.zeros(self.truss.elements.Ne)		
		
		for e in range(self.truss.elements.Ne):
			for n in range(self.Nh):
				o = -(n+1)**2*self.omega**2
				Ke = self.truss.Ke[e,:,:]
				Me = self.truss.Me[e,:,:]
				grad[e] += 2*np.real(self.adjoint[:,n].T.conjugate()@(o*Me+Ke)@self.cv[:,n])
				
		return peak_power, grad
	
